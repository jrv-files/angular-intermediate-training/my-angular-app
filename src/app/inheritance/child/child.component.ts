import { Component } from '@angular/core';
import { ParentComponent } from '../parent/parent.component';

@Component({
  selector: 'app-child',
  templateUrl: './child.component.html',
  styles: [
  ]
})
export class ChildComponent extends ParentComponent {

  constructor() {
    super();
    console.log('ChildComponent constructor');
  }

  override ngOnInit(): void {
    super.ngOnInit();
    console.log('ChildComponent ngOnInit');
  }

}
