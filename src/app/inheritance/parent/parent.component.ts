import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-parent',
  templateUrl: './parent.component.html',
  styles: [
  ]
})
export class ParentComponent implements OnInit {

  constructor() { 
    console.log('ParentComponent constructor');
  }

  ngOnInit(): void {
    console.log('ParentComponent ngOnInit');
  }

}
