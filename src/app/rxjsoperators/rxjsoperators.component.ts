import { Component, OnInit } from '@angular/core';
import { filter, from, map, merge, of } from 'rxjs';


@Component({
  selector: 'app-rxjsoperators',
  templateUrl: './rxjsoperators.component.html',
  styles: [
  ]
})
export class RxjsoperatorsComponent implements OnInit {
  constructor() { }


  ngOnInit(): void {
    let fruits = ['Apple', 'Orange', 'Banana', 'Mango', 'Grapes'];


    console.log('creation');
    const source1 = from(fruits);
    source1.subscribe(d=>console.log(d));


    const source2 = of(fruits);
    source2.subscribe(d=>console.log(d));


    console.log('combination');
    const numbers1 = of(1, 2, 3, 4, 5);
    const numbers2 = of(6, 7, 8, 9, 10);


    const numbers3 = merge(numbers1, numbers2);
    numbers3.subscribe(d=>console.log(d));


    console.log('filtration');
    const oddNumbers = numbers3.pipe(filter(n => n % 2 > 0));
    oddNumbers.subscribe(d=>console.log(d));


    console.log('transformation');
    const numbers4 = oddNumbers.pipe(map(n => n * n));
    numbers4.subscribe(d=>console.log(d));
  }
}




