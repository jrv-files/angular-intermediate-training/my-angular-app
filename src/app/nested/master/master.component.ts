import { Component, OnInit } from '@angular/core';
import { Post } from 'src/app/models/Post';
import { DataService } from 'src/app/services/data.service';


@Component({
  selector: 'app-master',
  templateUrl: './master.component.html',
  styles: [
  ]
})
export class MasterComponent implements OnInit {
  post: Post | undefined;
  id: number;
  commentCount: number | undefined;
  constructor(private dataService: DataService) {
    this.id = 1;
  }


  ngOnInit(): void {
    this.dataService.GetPost(this.id).subscribe(response => {
      this.post = response;
    });
  }
  GetCounter(counter: number) {
    this.commentCount = counter;
  }
}








