import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Comment } from 'src/app/models/Comment';
import { DataService } from 'src/app/services/data.service';


@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styles: [
  ]
})
export class DetailsComponent implements OnInit {
  @Input() id: number | undefined;
  @Output() counter: EventEmitter<number> = new EventEmitter<number>();
  comments: Comment[] = [];
  constructor(private dataService: DataService) { }


  ngOnInit(): void {
    if (this.id != undefined) {
      {
        this.dataService.GetComments(this.id).subscribe(response => {
          if (response != null) {
            // this.comments = response;
            //this.counter.emit(this.comments.length);
          }
        });
      }
    }
  }
  UpdateCounter() {
    this.counter.emit(this.comments.length);
  }
}










