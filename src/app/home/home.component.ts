import { Component } from "@angular/core";

@Component({
    selector: 'app-home',
    template: `
    <h1>Home Component</h1>
    <p>This is the home component</p>
    `
})
export class HomeComponent {

}