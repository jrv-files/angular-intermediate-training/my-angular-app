import { ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { ChildcdComponent } from '../childcd/childcd.component';

@Component({
  selector: 'app-parentcd',
  templateUrl: './parentcd.component.html',
  styles: [
  ]
})
export class ParentcdComponent implements OnInit {
  @ViewChild(ChildcdComponent) child: ChildcdComponent | any;
  constructor(private cd: ChangeDetectorRef) {
   
  }

  ngOnInit(): void {
  }
  ngAfterViewInit() {
    this.child.message = "Hello from Parent";
    //this.cd.detectChanges(); //not needed with on-push
  }

}
