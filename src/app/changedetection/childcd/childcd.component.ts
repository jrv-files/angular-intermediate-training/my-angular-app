import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-childcd',
  templateUrl: './childcd.component.html',
  styles: [
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ChildcdComponent implements OnInit {
  message: string | undefined;
  constructor() { }

  ngOnInit(): void {
  }
  triggerCD(){
    console.log('Child CD triggered');
  }
}
