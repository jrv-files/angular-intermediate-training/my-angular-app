import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';


@Component({
  selector: 'app-modelform',
  templateUrl: './modelform.component.html',
  styles: [
  ]
})
export class ModelformComponent implements OnInit {
  userForm: FormGroup;
  constructor(private fb: FormBuilder) {
    // this.userForm = new FormGroup({
    //   name: new FormControl('', Validators.required),
    //   email: new FormControl('', [Validators.required, Validators.email]),
    //   password: new FormControl('', [Validators.required]),
    //   confirmPassword: new FormControl('', Validators.required),
    //   contact: new FormControl('', Validators.pattern("^[789]\\d{9}$"))
    // });
    this.userForm = this.fb.group({
      name: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required]],
      confirmPassword: ['', Validators.required],
      contact: ['', Validators.pattern("^[789]\\d{9}$")]
    });
  }


  ngOnInit(): void {
  }
  
  SaveData(){
    if(this.userForm.valid){
      console.log(this.userForm.value);
    }
  }


}
