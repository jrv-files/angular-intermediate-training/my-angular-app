import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dir',
  templateUrl: './dir.component.html',
  styles: [
    `.red-color{
      color: red;
    }`
  ]
})
export class DirComponent implements OnInit {
  num: number;
  alphabet: string;

  colors: string[];
  color: string;
  constructor() {
    this.num = 1;
    this.alphabet = 'a';

    this.color = '';
    this.colors = ['red', 'green', 'blue'];
  }

  ngOnInit(): void {
  }

}
