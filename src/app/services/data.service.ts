import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';


import { Post } from '../models/Post';
import { environment } from 'src/environments/environment';


@Injectable({
  providedIn: 'root'
})
export class DataService {


  constructor(private httpClient: HttpClient) { }


  GetPost(id: number): Observable<Post> {
    return this.httpClient.get<Post>(environment.apiAddress + '/posts/1');
  }
  GetComments(id: number): Observable<Comment[]> {
    return this.httpClient.get<Comment[]>(`${environment.apiAddress}/posts/${id}/comments`);
  }
}


