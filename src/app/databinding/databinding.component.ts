import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-databinding',
  templateUrl: './databinding.component.html',
  styles: [
  ]
})
export class DatabindingComponent implements OnInit {
  name: string;
  site: string;
  siteName: string;
  constructor() {
    this.name = "Mohan";
    this.site = "https://www.google.com";
    this.siteName = "Google";
  }
  ngOnInit(): void {
  }
  Greet() {
    alert("Hello " + this.name);
  }
}
