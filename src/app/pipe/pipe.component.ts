import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-pipe',
  templateUrl: './pipe.component.html',
  styles: [
  ]
})
export class PipeComponent implements OnInit {
  employees: any[];
  constructor() {
    this.employees = [
      { name: 'Mohan', address: 'Noida', salary: 10000, dob: new Date('12/12/1990') },
      { name: 'Ram', address: 'Delhi', salary: 8000, dob: new Date('12/12/1991') },
      { name: 'Girdhar', address: 'Noida', salary: 10000, dob: new Date('12/12/1992') }
    ]
  }

  ngOnInit(): void {
  }

}
