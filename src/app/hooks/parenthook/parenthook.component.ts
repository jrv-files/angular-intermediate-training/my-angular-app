import { HttpClient } from '@angular/common/http';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Post } from 'src/app/models/Post';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-parenthook',
  templateUrl: './parenthook.component.html',
  styles: [
  ]
})
export class ParenthookComponent implements OnInit, OnDestroy {
  id: number;
  post: Post | undefined;
  commentCount: number | undefined;
  subscription: Subscription | undefined;
  constructor(private httpClient: HttpClient) {
    console.log('Parent Constructor');
    this.id = 1;
  }

  ngOnInit(): void {
    console.log('Parent ngOnInit');
    this.subscription = this.httpClient.get<Post>(environment.apiAddress + '/posts/' + this.id).subscribe(response => {
      this.post = response;
    });
  }
  GetCounter(counter: number) {
    this.commentCount = counter;
  }

  GetPost(): void {
    this.subscription = this.httpClient.get<Post>(environment.apiAddress + '/posts/' + this.id).subscribe(response => {
      this.post = response;
    });
  }
  ngOnDestroy(): void {
    console.log('Parent ngOnDestroy');
    this.subscription?.unsubscribe();
  }
}
