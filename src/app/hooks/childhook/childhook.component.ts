import { HttpClient } from '@angular/common/http';
import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Comment } from 'src/app/models/Comment';
@Component({
  selector: 'app-childhook',
  templateUrl: './childhook.component.html',
  styles: [
  ]
})
export class ChildhookComponent implements OnInit, OnChanges {
  @Input() id: number | undefined;
  @Output() counter: EventEmitter<number> = new EventEmitter<number>();
  comments: Comment[] = [];
  constructor(private httpClient: HttpClient) { }

  ngOnChanges(changes: SimpleChanges): void {
      console.log(changes);
      this.httpClient.get<Comment[]>(`${environment.apiAddress}/posts/${this.id}/comments`).subscribe(response => {
        if (response != null){
          this.comments = response;
          this.counter.emit(this.comments.length);
        }
      });
  }
  ngOnInit(): void {
    this.httpClient.get<Comment[]>(`${environment.apiAddress}/posts/${this.id}/comments`).subscribe(response => {
      if (response != null){
        this.comments = response;
        this.counter.emit(this.comments.length);
      }
    });
  }
}
