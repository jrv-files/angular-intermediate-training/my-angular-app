import { AfterViewInit, ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { MessageComponent } from '../message/message.component';

@Component({
  selector: 'app-parentview',
  templateUrl: './parentview.component.html',
  styles: [
  ]
})
export class ParentviewComponent implements OnInit, AfterViewInit {
  @ViewChild(MessageComponent) child: MessageComponent | any;
  constructor(private cd:ChangeDetectorRef) {

   }

  ngOnInit(): void {
  }
  ngAfterViewInit(){
    this.child.message = "Hello from Parent";
    this.cd.detectChanges();
  }
}
