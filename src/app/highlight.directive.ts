import { Directive, ElementRef, HostListener } from '@angular/core';

@Directive({
  selector: '[appHighlight],.appHighlight,#appHighlight'
})
export class HighlightDirective {

  constructor(private element: ElementRef) { }
  @HostListener('mouseover') f1() {
    this.element.nativeElement.style.backgroundColor = 'yellow';
  }
  @HostListener('mouseleave') f2() {
    this.element.nativeElement.style.backgroundColor = '';
  }
  @HostListener('click') f3() {
    let value = this.element.nativeElement.innerText;
    alert(value);
  }
}
