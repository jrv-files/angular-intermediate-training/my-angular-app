import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { DatabindingComponent } from './databinding/databinding.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DirComponent } from './dir/dir.component';
import { HighlightDirective } from './highlight.directive';
import { PipeComponent } from './pipe/pipe.component';
import { ReversePipe } from './reverse.pipe';
import { NotfoundComponent } from './notfound/notfound.component';
import { ProductComponent } from './product/product.component';
import { CategoryComponent } from './category/category.component';
import { AccountComponent } from './account/account.component';
import { ProfileComponent } from './profile/profile.component';
import { MembershipComponent } from './membership/membership.component';
import { ParentComponent } from './inheritance/parent/parent.component';
import { ChildComponent } from './inheritance/child/child.component';
import { MasterComponent } from './nested/master/master.component';
import { DetailsComponent } from './nested/details/details.component';
import { ParentviewComponent } from './viewchilds/parentview/parentview.component';
import { MessageComponent } from './viewchilds/message/message.component';
import { MainComponent } from './projection/main/main.component';
import { FormComponent } from './projection/form/form.component';
import { ParenthookComponent } from './hooks/parenthook/parenthook.component';
import { ChildhookComponent } from './hooks/childhook/childhook.component';
import { ParentcdComponent } from './changedetection/parentcd/parentcd.component';
import { ChildcdComponent } from './changedetection/childcd/childcd.component';
import { TemplateformComponent } from './templateform/templateform.component';
import { ModelformComponent } from './modelform/modelform.component';
import { RxjsoperatorsComponent } from './rxjsoperators/rxjsoperators.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    DatabindingComponent,
    DirComponent,
    HighlightDirective,
    PipeComponent,
    ReversePipe,
    NotfoundComponent,
    ProductComponent,
    CategoryComponent,
    AccountComponent,
    ProfileComponent,
    MembershipComponent,
    ParentComponent,
    ChildComponent,
    MasterComponent,
    DetailsComponent,
    ParentviewComponent,
    MessageComponent,
    MainComponent,
    FormComponent,
    ParenthookComponent,
    ChildhookComponent,
    ParentcdComponent,
    ChildcdComponent,
    TemplateformComponent,
    ModelformComponent,
    RxjsoperatorsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
