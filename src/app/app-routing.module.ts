import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { HomeComponent } from './home/home.component';
import { DatabindingComponent } from './databinding/databinding.component';
import { DirComponent } from './dir/dir.component';
import { PipeComponent } from './pipe/pipe.component';
import { NotfoundComponent } from './notfound/notfound.component';
import { ProductComponent } from './product/product.component';
import { CategoryComponent } from './category/category.component';
import { AccountComponent } from './account/account.component';
import { ProfileComponent } from './profile/profile.component';
import { MembershipComponent } from './membership/membership.component';
import { ChildComponent } from './inheritance/child/child.component';
import { MasterComponent } from './nested/master/master.component';
import { ParentviewComponent } from './viewchilds/parentview/parentview.component';
import { MainComponent } from './projection/main/main.component';
import { ParenthookComponent } from './hooks/parenthook/parenthook.component';
import { ParentcdComponent } from './changedetection/parentcd/parentcd.component';
import { TemplateformComponent } from './templateform/templateform.component';
import { ModelformComponent } from './modelform/modelform.component';
import { RxjsoperatorsComponent } from './rxjsoperators/rxjsoperators.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'binding', component: DatabindingComponent },
  { path: 'dir', component: DirComponent },
  { path: 'pipe', component: PipeComponent },
  { path: 'product/:id', component: ProductComponent },
  { path: 'product', component: ProductComponent },
  { path: 'category', component: CategoryComponent },
  { path: 'inheritance', component: ChildComponent },
  { path: 'nested', component: MasterComponent },
  { path: 'viewchild', component: ParentviewComponent },
  { path: 'projection', component: MainComponent },
  { path: 'hooks', component: ParenthookComponent },
  { path: 'cd', component: ParentcdComponent },
  { path: 'templateform', component: TemplateformComponent },
  { path: 'modelform', component: ModelformComponent },
  { path: 'rxjs', component: RxjsoperatorsComponent },

  {
    path: 'account', component: AccountComponent, children: [
      { path: 'profile', component: ProfileComponent },
      { path: 'membership', component: MembershipComponent },
    ]
  },
  { path: 'notfound', component: NotfoundComponent },
  { path: '**', redirectTo: 'notfound' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  //imports: [RouterModule.forRoot(routes, {useHash: true})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
